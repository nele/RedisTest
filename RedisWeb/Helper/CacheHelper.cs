﻿using CacheManager.Core;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedisWeb.Helper
{
    public class CacheHelper
    {
        private static object lockObj = new object();

        private static ICacheManager<object> _cache;

        private static TimeSpan _processCacheTime = TimeSpan.FromHours(1);

        private static TimeSpan _redisCacheTime = TimeSpan.FromDays(1);

        private static Lazy<ConnectionMultiplexer> lazyConnection = null;


        /// <summary>
        /// 初始化二级缓存管理
        /// 本地内存缓存和远程Redis缓存,默认内存过期时间 默认redis过期时间
        /// </summary>
        /// <param name="cacheName">缓存配置名称</param>
        /// <param name="redisConfiguration">redis服务器配置信息</param>
        /// <e
        public static void Init(string cacheName, string redisConfiguration)
        {
            Init(cacheName, redisConfiguration, _processCacheTime, _redisCacheTime);
        }

        /// <summary>
        /// 初始化二级缓存管理
        /// 本地内存缓存和远程Redis缓存
        /// </summary>
        /// <param name="cacheName">缓存配置名称</param>
        /// <param name="redisConfiguration">redis服务器配置信息</param>
        /// <param name="processCacheTime">内存缓存过期时间</param>
        /// <param name="redisCacheTime">redis缓存过期时间</param>
        public static void Init(string cacheName, string redisConfiguration, TimeSpan processCacheTime, TimeSpan redisCacheTime)
        {

            lock (lockObj)
            {
                if (_cache == null)
                {
                    RedisConfig redisConfig = new RedisConfig(redisConfiguration);

                    lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
                    {
                        return ConnectionMultiplexer.Connect(redisConfiguration);
                    });
                    //配置二级缓存
                    _cache = CacheFactory.Build(cacheName, settings =>
                    {
                        settings.WithUpdateMode(CacheUpdateMode.Up)
                            .WithRedisConfiguration("redis", config =>//Redis缓存配置
                            {
                                config.WithAllowAdmin()
                                    .WithDatabase(0)
                                    .WithEndpoint(redisConfig.Host, redisConfig.Port);
                            })
                            .WithMaxRetries(100)//尝试次数
                            .WithRetryTimeout(1000)//尝试超时时间
                            .WithRedisBackplane("redis")
                            //.WithRedisBackPlate("redis")//redis使用Back Plate
                            .WithRedisCacheHandle("redis", true)//redis缓存handle
                            .WithExpiration(ExpirationMode.Sliding, redisCacheTime);//Redis缓存时间
                    });
                }
            }
        }

        /// <summary>
        /// 添加或者更新键值
        /// </summary>
        /// <typeparam name="T">数据类型</typeparam>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        public static void AddOrUpdate<T>(string key, T value, string region = "IMS")
        {
            _cache.Put(key, value, region);
        }

        /// <summary>
        /// 移除键
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>移除成功标志</returns>
        public static bool Remove(string key, string region = "IMS")
        {
            return _cache.Remove(key, region);
        }

        /// <summary>
        /// 清空所有缓存
        /// </summary>
        public static void Clear()
        {
            _cache.Clear();
        }

        /// <summary>
        /// 读取缓存值
        /// </summary>
        /// <typeparam name="T">数据类型</typeparam>
        /// <param name="key">键</param>
        /// <returns>缓存值</returns>
        public static T Get<T>(string key, string region = "IMS")
        {
            return _cache.Get<T>(key, region);
        }

        public static ConnectionMultiplexer redis
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }

    /// <summary>
    /// Redis服务配置
    /// </summary>
    internal class RedisConfig
    {
        public RedisConfig(string redisConfiguration)
        {
            if (string.IsNullOrEmpty(redisConfiguration))
            {
                throw new ArgumentNullException();
            }
            string[] arrConfig = redisConfiguration.Split(':');
            if (arrConfig.Length != 2)
            {
                throw new ArgumentException();
            }
            Host = arrConfig[0];
            Port = Convert.ToInt32(arrConfig[1]);
        }

        /// <summary>
        /// 主机
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }
    }
}