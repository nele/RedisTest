﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedisWeb.Helper
{
    public class RedisHelper
    {
        public static readonly RedisHelper Instance = new RedisHelper();

        private readonly object _syncRoot = new object();

        //private RedisClient RedisClient { get; set; }
        private RedisHelper()
        {

        }

        public ConnectionMultiplexer RedisConnectionManager
        {
            get; private set;
        }

        private IDatabase RedisDatabase { get; set; }

        public void Init(string configuration)
        {
            try
            {

                //ConfigurationOptions options = new ConfigurationOptions()
                //{
                //    ConnectTimeout = 1000 * 3600,
                //    ResponseTimeout = 1000 * 3600,
                //    SyncTimeout = 1000 * 3600
                //};
                this.RedisConnectionManager = ConnectionMultiplexer
                    .Connect(configuration);
                this.RedisDatabase = this.RedisConnectionManager.GetDatabase();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void UnInit()
        {
            this.RedisDatabase = null;
            this.RedisConnectionManager.Dispose();
        }

        public void HSet(string mapName, string key, string value)
        {
            lock (this._syncRoot)
            {
                //this.RedisClient.HSet(mapName, GetBytes(key), GetBytes(value));
                this.RedisDatabase.HashSet(mapName, key, value);
            }
        }

        public string HGet(string mapName, string key)
        {
            lock (this._syncRoot)
            {
                //byte[] value = this.RedisClient.HGet(mapName, GetBytes(key));
                //if (value == null || value.Length == 0)
                //{
                //	return null;
                //}
                //string str = GetString(value);
                //return str;
                string str = this.RedisDatabase.HashGet(mapName, key);
                return str;
            }
        }

        public IDictionary<string, string> HGetAll(string mapName)
        {
            lock (this._syncRoot)
            {
                //byte[][] values = this.RedisClient.HGetAll(mapName);
                //int count = values.Length / 2;
                //IDictionary<string, string> dic = new Dictionary<string, string>(count);
                //for (int i = 0; i < values.Length;)
                //{
                //	string key = GetString(values[i++]);
                //	string value = GetString(values[i++]);
                //	dic[key] = value;
                //}

                //return dic;
                HashEntry[] entries = this.RedisDatabase.HashGetAll(mapName);
                IDictionary<string, string> dic = new Dictionary<string, string>();
                for (int i = 0; i < entries.Length; i++)
                {
                    HashEntry entry = entries[i];
                    string key = entry.Name;
                    string value = entry.Value;
                    dic[key] = value;
                }

                return dic;
            }
        }

        //private readonly Random _radom = new Random();

        public IDictionary<string, string> HScan(string mapName, string pattern, int size)
        {
            lock (this._syncRoot)
            {
                IEnumerable<HashEntry> entries = this.RedisDatabase.HashScan(
                    mapName, pattern, 10, CommandFlags.None);
                if (entries == null)
                {
                    throw new ApplicationException("HSCAN 命令出错，返回空");
                }
                IDictionary<string, string> dic = new Dictionary<string, string>();
                foreach (HashEntry entry in entries)
                {
                    string key = entry.Name;
                    string value = entry.Value;
                    dic[key] = value;
                    if (--size <= 0)
                    {
                        break;
                    }
                }

                return dic;
            }
        }

        public int HDel(string mapName, string key)
        {
            lock (this._syncRoot)
            {
                //int value = this.RedisClient.HDel(mapName, GetBytes(key));
                bool flag = this.RedisDatabase.HashDelete(mapName, key);
                return flag ? 1 : 0;
            }
        }

        public void SAdd(string setName, string value)
        {
            lock (this._syncRoot)
            {
                bool flag = this.RedisDatabase.SetAdd(setName, value);
            }
        }

        public long SAdd(string setName, IList<string> values)
        {
            lock (this._syncRoot)
            {
                RedisValue[] rvs = values.Select(p => (RedisValue)p).ToArray();
                long count = this.RedisDatabase.SetAdd(setName, rvs);
                return count;
            }
        }

        public IList<string> SIntersect(IList<string> setNames)
        {
            lock (this._syncRoot)
            {
                RedisKey[] keys = setNames.Select(p => (RedisKey)(p)).ToArray();
                RedisValue[] values = this.RedisDatabase.SetCombine(SetOperation.Intersect, keys);
                IList<string> list = values.Select(p => (string)p).ToList();
                return list;
            }
        }

        public IList<string> SScan(string sname, string pattern, int offset, int count)
        {
            IList<string> list = new List<string>();
            lock (this._syncRoot)
            {
                IEnumerable<RedisValue> iter = this.RedisDatabase.SetScan(sname, pattern, 10);
                foreach (var item in iter)
                {
                    if (offset > 0)
                    {
                        --offset;
                        continue;
                    }
                    if (count > 0)
                    {
                        --count;
                        list.Add((string)item);
                    }
                }
            }
            return list;
        }

        public long SCard(string setName)
        {
            long result = this.RedisDatabase.SetLength(setName);
            return result;
        }

        public bool KeyExpire(string key, TimeSpan? ts)
        {
            lock (this._syncRoot)
            {
                bool flag = this.RedisDatabase.KeyExpire(key, ts);
                return flag;
            }
        }

        //private string GetString(byte[] bytes)
        //{
        //	return _encoding.GetString(bytes);
        //}

        //private byte[] GetBytes(string str)
        //{
        //	return _encoding.GetBytes(str);
        //}
    }
}