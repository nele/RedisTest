﻿using RedisSessionProvider.Config;
using RedisWeb.Helper;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RedisWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            string RedisServer = string.Format("{0}:{1}", "127.0.0.1", "6379");

            //配置session保存到redis
            ConfigurationOptions redisConfigOpts = ConfigurationOptions.Parse(RedisServer);
            RedisConnectionConfig.GetSERedisServerConfig = (HttpContextBase context) =>
            {
                return new KeyValuePair<string, ConfigurationOptions>(
                    "DefaultConnection",
                    redisConfigOpts);
            };

            //RedisHelper redis = RedisHelper.Instance;
            //redis.Init(RedisServer);

            CacheHelper.Init("Citms.Cache", RedisServer);
        }
    }
}
